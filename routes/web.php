<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Victor deleted L21 - giving error..?
Route::get('/home', 'ProfileController@index')->name('home');


Route::get('/profile/{profile}', 'ProfileController@index')->name('profile.show');
Route::get('/profile/{profile}/edit', 'ProfileController@edit')->name('profiles.edit');
Route::patch('/profile/{profile}', 'ProfileController@update')->name('profiles.update');


Route::get('/images/create','ImagesController@create')->name('images.create');
Route::post('/images/store','ImagesController@store')->name('images.store');

Route::get('/profiles/create','ProfileController@create')->name('profiles.create');
Route::post('/profiles/store','ProfileController@store')->name('profiles.store');




//Testing route syntax below-----------------------------------------------

Route::get('/test1', function(){
	return "<h2>This direct html test has worked!</h2>";	
});

Route::get('/test2',function(){
	$name = "Oscar";
	return view('stuff',compact('name'));

});

