<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class ImagesController extends Controller
{
    public function create()
    {
        $auth_user = Auth::user();
    	return view('images.create', ['auth_user' => $auth_user]);
    }




    // Store new image on database
    public function store(Request $request)
    {
        $user = Auth::user();

        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|image',
        ]);
       
        $image = new Image([
            'url' => $this->storeImage($request),
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'user_id' => $user->id
        ]);
       
        $image->save();
 
        return redirect('/profile/{profile}')->with('success', 'Data added');
    }
 
// Format and store image
    public function storeImage(Request $request)
    {
        $file = $request->file('image');
        $extension = $file->getClientOriginalExtension();
        Storage::disk('public')->put($file->getFilename().'.'.$extension,  File::get($file));
 
        return $image = $file->getFilename().'.'.$extension;
    }















    // public function store(Request $request)
    // {
    //     $user = Auth::user();

    // 	$request->validate([
    //         'title' => 'required',
    // 		'description' => 'required',
    // 		'image' => 'required',
    //     ]);
        
        
    //     $file = $request->file('image');

    //     $fileName = $file->getClientOriginalName();

    //     $destinationPath = 'astro_images/';
    //     Storage::disk('local')->put($fileName, File::get($file));
    //     $file->move($destinationPath, $fileName);


    //     $image = new Image([
    //         'title' => $request->get('title'),
    //         'url' => Storage::url($fileName),
    //         'description' => $request->get('description'),
    //         'user_id' => $user->id
    //     ]);        
        

    //     $image->save();

    //     //return redirect('profile.show', compact('file'=>$fileName));
    //     return redirect('/profile/{profile}')->with('success', 'New image added');
    // }
}
