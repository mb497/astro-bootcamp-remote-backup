<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Profile;
//use Illuminate\Support\Facades\Storage;
//use Illuminate\Support\Facades\File;


class ProfileController extends Controller
{
	public function index()
    {
       	$auth_user = Auth::user();
        return view('profiles.index', ['auth_user' => $auth_user]);
    }

    public function create()
    {
        $auth_user = Auth::user();
    	return view('profiles.create', ['auth_user' => $auth_user]);
    }


    // Store new profile on database
    public function store(Request $request)
    {
        $user = Auth::user();

        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            ]);
       
        $profile = new Profile([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'user_id' => $user->id
        ]);
       
        $profile->save();
 
        return redirect('/profile/{profile}')->with('success', 'Data added');
    }

    public function edit()
    {
        $user = Auth::user();
        return view('profiles.edit',['user'=>$user]);

    }

    public function update()
    {
        $user = Auth::user();
        $data = request()->validate([
            'title' => 'required',
            'description' => 'required',
        ]);

        $user->profile->update($data);

        return redirect("/profile/{profile}");
    }
}
