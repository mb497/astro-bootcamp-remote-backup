<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class TruncateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    //See   https://laravel.com/docs/5.1/queries#deletes
    public function up()
    {
        DB::table('images')->truncate();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('images', function (Blueprint $table) {
            //
        });
    }
}