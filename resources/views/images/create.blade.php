@extends('layouts.app')

@section('content')

<div class='p-3'>
<form method="POST" action="{{ route('images.store') }}" enctype="multipart/form-data">
       {{ csrf_field() }}

       <div class="form-group pt-2">
           <label for="description">Title of astrophotograph: </label>
           <textarea class="form-control" rows="3" name="title" placeholder="Input title here"></textarea>
       </div>

        <div class="form-group pt-3">
           <label for="description">Description to display below image: </label>
           <textarea class="form-control" rows="3" name="description" placeholder="Input description here"></textarea>
       </div>

       <div class="form-group pt-3">
           <label for="image">Astrophotograph to upload: </label>
           <input type="file" class="form-control-file" name="image"> <!--Sotos: 'name' is the actual key with which to reference in file save-->
       </div>

       <button type="submit" class="btn btn-primary">SUBMIT</button>
       <a href="/home" class="btn btn-primary">Back</a>
</form>
</div>

@endsection