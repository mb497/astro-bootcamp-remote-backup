@extends('layouts.app')

@section('content')
<div class="card mb-3 bg-white border-0 p-2">
      <div class="row no-gutters">
        <div class="col-md-4">
          <img src="/astro_images/obsy.jpg" class="card-img rounded-circle" alt="HOME IMAGE">
        </div>
        <div class="col-md-8">

            <div class="card-body">

            <div class="d-flex justify-content-between align-items-baseline">
            <h2 class="card-title">{{ $auth_user->username }}</h2>
            <h3 class="card-title"><a href="{{ route('images.create') }}">Add new image</a></h3>
            </div>

            <h5 class="card-title"><a href="{{ route('profiles.create') }}">Add profile details</a></h5>
            <h5 class="card-title"><a href="/profile/{{ $auth_user->id }}/edit">Edit profile</a></h5>

            <h3 class="card-text">{{ $auth_user->profile->title ?? "!Not entered!"}}</h3>
            <h3 class="card-text">{{ $auth_user->profile->description ?? "!Not entered!" }}</h3>
            <p class="card-text"><strong>12</strong> posts</p>
            </div>

        </div>
      </div>
</div>

<div class="card-deck">

    @foreach ($auth_user->images as $image)
    <div class="card">
        <!-- <img src='{{$image->url}}' class="card-img-top" alt="IMAGE FROM DB"> -->
        <img src='{{"/astro_images/" . $image->url}}' class="card-img-top" alt="{{$image->url}}">
        <div class="card-body">
          <h3 class="card-title">{{ $image->title }}</h3>
          <p class="card-text">{{ $image->description }}</p>
        </div>
    </div>
    @endforeach
</div>

<div class="card pt-3">
      <div class="card-header">
        A motivational quote
      </div>
      <div class="card-body">
        <blockquote class="blockquote mb-0">
          <p>Science is interesting, and if you don’t agree, you can f*ck off.</p>
          <footer class="blockquote-footer">Richard Dawkins DPhil FRS<cite title="Source Title"> New Scientist Magazine</cite></footer>
        </blockquote>
      </div>
</div>

@endsection