@extends('layouts.app')

@section('content')

<div class='p-3'>
<form method="POST" action="{{ route('profiles.store') }}" enctype="multipart/form-data">
       {{ csrf_field() }}

       <div class="form-group p-2">
           <label for="description">Profile title: </label>
           <textarea class="form-control" rows="3" name="title" placeholder="Input title here"></textarea>
       </div>

        <div class="form-group p-3">
           <label for="description">Profile description: </label>
           <textarea class="form-control" rows="3" name="description" placeholder="Input description here"></textarea>
       </div>
     
       <button type="submit" class="btn btn-primary">SUBMIT</button>
       <a href="/home" class="btn btn-primary">Back</a>
</form>
</div>

@endsection