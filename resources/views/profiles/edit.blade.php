@extends('layouts.app')

@section('content')
<div class='container'>

	<!--"PATCH" preferable to "PUT" verb, though trickery req'd for the HTTP. See L9 for blade override-->
	<form method="POST" action="/profile/{{ $user->id }}" enctype="multipart/form-data">
	       {{ csrf_field() }}
	       @method('PATCH')

	       <div class="row">
	       		<h1>Edit Profile</h1>
	       </div>

	       <div class="form-group p-2">
	           <label for="description">Profile title (max 20 characters): </label>
	           <textarea class="form-control" rows="3" name="title" placeholder="{{ old('title') ?? $user->profile->title }}"></textarea>
	       </div>

	        <div class="form-group p-3">
	           <label for="description">Profile description: </label>
	           <textarea class="form-control" rows="3" name="description" placeholder="{{ old('description') ?? $user->profile->description }}"></textarea>
	       </div>
	     
	       <button type="submit" class="btn btn-primary">Save Profile</button>
	       <a href="/home" class="btn btn-primary">Back</a>
	</form>

</div>


<!-- <script type="text/javascript" src="/js/app.js">

		// Constructor function for a Validator class.
		function Validator(title)
		{
			// store the data that it validates as properties
			this.title = title;

			// returns true if both the title is OK
			this.isOk = function ()
			{
				return this.title.length > 0 && this.title.length < 21;
			};
		}

		// wire up the event handlers when this func is called
		function bindToEvents()
		{
			// before the form data is sent to the server, check that the
			// form is OK.
			document.getElementById('edit_form').onsubmit = function (event)
			{
				// extract the value of the data to validate
				var validator = new Validator(this.title.value);
				if (!validator.isOk())
				{
					alert('Warning! your input is bad!');
					
					// prevent the form from sending data to the server!
					event.preventDefault();
				}
			};
		}

		// invoke the function that will wire up the event handlers
		bindToEvents();

	</script> -->


@endsection